--[[
    GD50 2018
    Pong Remake

    -- Paddle Class --

    Author: Colton Ogden
    cogden@cs50.harvard.edu

    Represents a paddle that can move up and down. Used in the main
    program to deflect the ball back toward the opponent.
]]

DIRECTION_NONE = 0
DIRECTION_UP = 1
DIRECTION_DOWN = 2

Paddle = Class{}

--[[
    The `init` function on our class is called just once, when the object
    is first created. Used to set up all variables in the class and get it
    ready for use.

    Our Paddle should take an X and a Y, for positioning, as well as a width
    and height for its dimensions.

    Note that `self` is a reference to *this* object, whichever object is
    instantiated at the time this function is called. Different objects can
    have their own x, y, width, and height values, thus serving as containers
    for data. In this sense, they're very similar to structs in C.
]]
function Paddle:init(x, y, width, height)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.dy = 0
    -- used track num consequent frames in the same direction, for cool bouncing
    self.framesSameDir = 0
    self.lastDir = DIRECTION_NONE
end

function Paddle:update(dt)
    -- math.max here ensures that we're the greater of 0 or the player's
    -- current calculated Y position when pressing up so that we don't
    -- go into the negatives; the movement calculation is simply our
    -- previously-defined paddle speed scaled by dt
    self.y = self.y + self.dy * dt
    if self.dy < 0 and self.y < 0 then
        self.y = 0
        self.framesSameDir = 0
    -- similar to before, this time we use math.min to ensure we don't
    -- go any farther than the bottom of the screen minus the paddle's
    -- height (or else it will go partially below, since position is
    -- based on its top left corner)
    elseif self.dy > 0 and self.y > (VIRTUAL_HEIGHT - self.height) then
        self.y = VIRTUAL_HEIGHT - self.height
        self.framesSameDir = 0
    end
end

function Paddle:updateOnMove(pickedUp, pickedDown, maxFrames)
    if pickedUp then
        if self.lastDir == DIRECTION_UP then
            self.framesSameDir = math.min(self.framesSameDir+1, maxFrames)
        end
        self.lastDir = DIRECTION_UP
    elseif pickedDown then
        if self.lastDir == DIRECTION_DOWN then
            self.framesSameDir = math.min(self.framesSameDir+1, maxFrames)
        end
        self.lastDir = DIRECTION_DOWN
    else
        self.lastDir = DIRECTION_NONE
        self.framesSameDir = 0
    end
end

function Paddle:resetInternals()
    self.lastDir = DIRECTION_NONE
    self.framesSameDir = 0
end

--[[
    To be called by our main function in `love.draw`, ideally. Uses
    LÖVE2D's `rectangle` function, which takes in a draw mode as the first
    argument as well as the position and dimensions for the rectangle. To
    change the color, one must call `love.graphics.setColor`. As of the
    newest version of LÖVE2D, you can even draw rounded rectangles!
]]
function Paddle:render()
    love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
end