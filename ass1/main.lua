--[[
    GD50 2018
    Pong Remake

    -- Main Program --

    Author: Colton Ogden
    cogden@cs50.harvard.edu

    Originally programmed by Atari in 1972. Features two
    paddles, controlled by players, with the goal of getting
    the ball past your opponent's edge. First to 10 points wins.

    This version is built to more closely resemble the NES than
    the original Pong machines or the Atari 2600 in terms of
    resolution, though in widescreen (16:9) so it looks nicer on 
    modern systems.
]]

-- push is a library that will allow us to draw our game at a virtual
-- resolution, instead of however large our window is; used to provide
-- a more retro aesthetic
--
-- https://github.com/Ulydev/push
push = require 'push'

-- the "Class" library we're using will allow us to represent anything in
-- our game as code, rather than keeping track of many disparate variables and
-- methods
--
-- https://github.com/vrld/hump/blob/master/class.lua
Class = require 'class'

-- our Paddle class, which stores position and dimensions for each Paddle
-- and the logic for rendering them
require 'Paddle'

-- our Ball class, which isn't much different than a Paddle structure-wise
-- but which will mechanically function very differently
require 'Ball'

-- size of our actual window
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

-- size we're trying to emulate with push
VIRTUAL_WIDTH = 432
VIRTUAL_HEIGHT = 243

-- paddle movement speed
PADDLE_SPEED = 200

-- use map to set the keys that control players
player1Controls = {upward = 'w',downward = 's'}
player2Controls = {upward = 'up', downward = 'down'}

CONTROLLER_TYPE_HUMAN = 0
CONTROLLER_TYPE_AI_BASIC = 1
CONTROLLER_TYPE_AI_ADVANCED = 2
-- set which player is controlled by AI or humans
AI_MOVEMENT_TOLERANCE = 5
controllerType = {CONTROLLER_TYPE_AI_BASIC, CONTROLLER_TYPE_AI_ADVANCED}
BOUNCE_SPEED_FACTOR = 1.03

-- cool bouncing parameters
doCoolBouncing = true
CB_MAX_ANGLE = 50.0
base_ball_speed = 0
CB_BOUNCE_SPEED_FACTOR = 1.05
DEG_TO_RAD = math.pi / 180.0

ENUM_DIR_NONE = 0
ENUM_DIR_UP = 1
ENUM_DIR_DOWN = 2

CB_MAX_MOMENTUM_COEFF = 2.0
MAX_FRAMES_MOMENTUM = 90
momentumCoeffPerFrame = CB_MAX_MOMENTUM_COEFF / MAX_FRAMES_MOMENTUM

lastcoeff = 0.0
lastdt = 0


--[[
    Called just once at the beginning of the game; used to set up
    game objects, variables, etc. and prepare the game world.
]]
function love.load()
    -- set love's default filter to "nearest-neighbor", which essentially
    -- means there will be no filtering of pixels (blurriness), which is
    -- important for a nice crisp, 2D look
    love.graphics.setDefaultFilter('nearest', 'nearest')

    -- set the title of our application window
    love.window.setTitle('Pong')

    -- seed the RNG so that calls to random are always random
    math.randomseed(os.time())

    -- initialize our nice-looking retro text fonts
    smallFont = love.graphics.newFont('font.ttf', 8)
    largeFont = love.graphics.newFont('font.ttf', 16)
    scoreFont = love.graphics.newFont('font.ttf', 32)
    love.graphics.setFont(smallFont)

    -- set up our sound effects; later, we can just index this table and
    -- call each entry's `play` method
    sounds = {
        ['paddle_hit'] = love.audio.newSource('sounds/paddle_hit.wav', 'static'),
        ['score'] = love.audio.newSource('sounds/score.wav', 'static'),
        ['wall_hit'] = love.audio.newSource('sounds/wall_hit.wav', 'static')
    }
    
    -- initialize our virtual resolution, which will be rendered within our
    -- actual window no matter its dimensions
    push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
        fullscreen = false,
        resizable = true,
        vsync = true
    })

    -- initialize our player paddles; make them global so that they can be
    -- detected by other functions and modules
    player1 = Paddle(10, 30, 5, 20)
    player2 = Paddle(VIRTUAL_WIDTH - 10, VIRTUAL_HEIGHT - 30, 5, 20)

    -- place a ball in the middle of the screen
    ball = Ball(VIRTUAL_WIDTH / 2 - 2, VIRTUAL_HEIGHT / 2 - 2, 4, 4)

    -- initialize score variables
    player1Score = 0
    player2Score = 0

    -- either going to be 1 or 2; whomever is scored on gets to serve the
    -- following turn
    servingPlayer = 1

    -- player who won the game; not set to a proper value until we reach
    -- that state in the game
    winningPlayer = 0

    -- the state of our game; can be any of the following:
    -- 1. 'start' (the beginning of the game, before first serve)
    -- 2. 'serve' (waiting on a key press to serve the ball)
    -- 3. 'play' (the ball is in play, bouncing between paddles)
    -- 4. 'done' (the game is over, with a victor, ready for restart)
    gameState = 'start'
end

--[[
    Called whenever we change the dimensions of our window, as by dragging
    out its bottom corner, for example. In this case, we only need to worry
    about calling out to `push` to handle the resizing. Takes in a `w` and
    `h` variable representing width and height, respectively.
]]
function love.resize(w, h)
    push:resize(w, h)
end

--[[
    Called every frame, passing in `dt` since the last frame. `dt`
    is short for `deltaTime` and is measured in seconds. Multiplying
    this by any changes we wish to make in our game will allow our
    game to perform consistently across all hardware; otherwise, any
    changes we make will be applied as fast as possible and will vary
    across system hardware.
]]
function love.update(dt)
    if gameState == 'serve' then
        -- before switching to play, initialize ball's velocity based
        -- on player who last scored
        ball.dy = math.random(-50, 50)
        if servingPlayer == 1 then
            ball.dx = math.random(140, 200)
        else
            ball.dx = -math.random(140, 200)
        end
        base_ball_speed = computeBallSpeed(ball)
    elseif gameState == 'play' then
        -- detect ball collision with paddles, reversing dx if true and
        -- slightly increasing it, then altering the dy based on the position
        -- at which it collided, then playing a sound effect
        if ball:collides(player1) then
            ball.x = player1.x + 5
            if doCoolBouncing then
                local coeff = momentumCoeffPerFrame * player1.framesSameDir + CB_BOUNCE_SPEED_FACTOR
                --lastcoeff = momentumCoeffPerFrame * player1.framesSameDir + CB_BOUNCE_SPEED_FACTOR
                base_ball_speed = base_ball_speed * coeff
                local vx, vy = computeVelBouncing(player1, ball)
                ball.dx = vx
                if ball.dy < 0 then
                    ball.dy = -vy
                else
                    ball.dy = vy
                end
            else
                ball.dx = -ball.dx * BOUNCE_SPEED_FACTOR
                -- keep velocity going in the same direction, but randomize it
                if ball.dy < 0 then
                    ball.dy = -math.random(10, 150)
                else
                    ball.dy = math.random(10, 150)
                end
            end
            sounds['paddle_hit']:play()
        end
        if ball:collides(player2) then
            ball.x = player2.x - 4
            if doCoolBouncing then
                local coeff = momentumCoeffPerFrame * player2.framesSameDir + CB_BOUNCE_SPEED_FACTOR
                --lastcoeff = momentumCoeffPerFrame * player2.framesSameDir + CB_BOUNCE_SPEED_FACTOR
                base_ball_speed = base_ball_speed * coeff
                local vx, vy = computeVelBouncing(player2, ball)
                ball.dx = -vx
                if ball.dy < 0 then
                    ball.dy = -vy
                else
                    ball.dy = vy
                end
            else
                ball.dx = -ball.dx * BOUNCE_SPEED_FACTOR
                -- keep velocity going in the same direction, but randomize it
                if ball.dy < 0 then
                    ball.dy = -math.random(10, 150)
                else
                    ball.dy = math.random(10, 150)
                end
            end
            sounds['paddle_hit']:play()
        end

        handleBallBorderCollisions(ball, sounds)

        -- if we reach the left edge of the screen, go back to serve
        -- and update the score and serving player
        if ball.x < 0 then
            servingPlayer = 1
            player2Score = player2Score + 1
            sounds['score']:play()

            -- if we've reached a score of 10, the game is over; set the
            -- state to done so we can show the victory message
            if player2Score == 10 then
                winningPlayer = 2
                gameState = 'done'
            else
                gameState = 'serve'
                -- places the ball in the middle of the screen, no velocity
                ball:reset()
            end
        end

        -- if we reach the right edge of the screen, go back to serve
        -- and update the score and serving player
        if ball.x > VIRTUAL_WIDTH then
            servingPlayer = 2
            player1Score = player1Score + 1
            sounds['score']:play()

            -- if we've reached a score of 10, the game is over; set the
            -- state to done so we can show the victory message
            if player1Score == 10 then
                winningPlayer = 1
                gameState = 'done'
            else
                gameState = 'serve'
                -- places the ball in the middle of the screen, no velocity
                ball:reset()
            end
        end
    end

    --
    -- paddles can move no matter what state we're in
    --
    -- player 1
    local player1Up = false
    local player1Down = false
    if controllerType[1] == CONTROLLER_TYPE_HUMAN then
        player1Up, player1Down = keyboardController(player1Controls)
    elseif controllerType[1] == CONTROLLER_TYPE_AI_BASIC then
        player1Up, player1Down = AIControllerSimple(player1, ball)
    elseif controllerType[1] == CONTROLLER_TYPE_AI_ADVANCED  then
        player1Up, player1Down = AIControllerAdvanced(player1, ball)
    end
    if player1Up then
        player1:updateOnMove(true, false, MAX_FRAMES_MOMENTUM)
    elseif player1Down then
        player1:updateOnMove(false, true, MAX_FRAMES_MOMENTUM)
    else
        player1:updateOnMove(false, false, MAX_FRAMES_MOMENTUM)
    end
    updatePaddle(player1, player1Up, player1Down)

    -- player 2
    local player2Up = false
    local player2Down = false
    if controllerType[2] == CONTROLLER_TYPE_HUMAN then
        player2Up, player2Down = keyboardController(player2Controls)
    elseif controllerType[2] == CONTROLLER_TYPE_AI_BASIC then
        player2Up, player2Down = AIControllerSimple(player2, ball)
    elseif controllerType[2] == CONTROLLER_TYPE_AI_ADVANCED  then
        player2Up, player2Down = AIControllerAdvanced(player2, ball)
    end
    if player2Up then
        player2:updateOnMove(true, false, MAX_FRAMES_MOMENTUM)
    elseif player2Down then
        player2:updateOnMove(false, true, MAX_FRAMES_MOMENTUM)
    else
        player2:updateOnMove(false, false, MAX_FRAMES_MOMENTUM)
    end
    updatePaddle(player2, player2Up, player2Down)

    -- update our ball based on its DX and DY only if we're in play state;
    -- scale the velocity by dt so movement is framerate-independent
    if gameState == 'play' then
        ball:update(dt)
    end

    player1:update(dt)
    player2:update(dt)
    lastdt = dt
end

--[[
    A callback that processes key strokes as they happen, just the once.
    Does not account for keys that are held down, which is handled by a
    separate function (`love.keyboard.isDown`). Useful for when we want
    things to happen right away, just once, like when we want to quit.
]]
function love.keypressed(key)
    -- `key` will be whatever key this callback detected as pressed
    if key == 'escape' then
        -- the function LÖVE2D uses to quit the application
        love.event.quit()
    -- if we press enter during either the start or serve phase, it should
    -- transition to the next appropriate state
    elseif key == 'enter' or key == 'return' then
        if gameState == 'start' then
            gameState = 'serve'
        elseif gameState == 'serve' then
            gameState = 'play'
        elseif gameState == 'done' then
            -- game is simply in a restart phase here, but will set the serving
            -- player to the opponent of whomever won for fairness!
            gameState = 'serve'

            ball:reset()

            -- reset scores to 0
            player1Score = 0
            player2Score = 0
            player1:resetInternals()
            player2:resetInternals()

            -- decide serving player as the opposite of who won
            if winningPlayer == 1 then
                servingPlayer = 2
            else
                servingPlayer = 1
            end
        end
    end
end

--[[
    Called each frame after update; is responsible simply for
    drawing all of our game objects and more to the screen.
]]
function love.draw()
    -- begin drawing with push, in our virtual resolution
    push:apply('start')

    love.graphics.clear(40, 45, 52, 255)
    
    -- render different things depending on which part of the game we're in
    if gameState == 'start' then
        -- UI messages
        love.graphics.setFont(smallFont)
        love.graphics.printf('Welcome to Pong!', 0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Press Enter to begin!', 0, 20, VIRTUAL_WIDTH, 'center')
    elseif gameState == 'serve' then
        -- UI messages
        love.graphics.setFont(smallFont)
        love.graphics.printf('Player ' .. tostring(servingPlayer) .. "'s serve!", 
            0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Press Enter to serve!', 0, 20, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('AI target Y: ' .. tostring(AITargetY), 0, 40, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Ball dx: ' .. tostring(ball.dx), 0, 50, VIRTUAL_WIDTH, 'center')
    elseif gameState == 'play' then
        -- no UI messages to display in play
        local s = computeBallSpeed(ball)
        love.graphics.printf('Ball vel: ' .. tostring(s), 0, 30, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('AI target Y: ' .. tostring(AITargetY), 0, 40, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('AIWaitFrames: ' .. tostring(AIWaitFrames), 0, 50, VIRTUAL_WIDTH, 'center')
    elseif gameState == 'done' then
        -- UI messages
        love.graphics.setFont(largeFont)
        love.graphics.printf('Player ' .. tostring(winningPlayer) .. ' wins!',
            0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.setFont(smallFont)
        love.graphics.printf('Press Enter to restart!', 0, 30, VIRTUAL_WIDTH, 'center')
    end

    -- show the score before ball is rendered so it can move over the text
    displayScore()
    
    player1:render()
    player2:render()
    ball:render()

    -- display FPS for debugging; simply comment out to remove
    displayFPS()

    -- end our drawing to push
    push:apply('end')
end

function computeBallSpeed(ball)
    return math.sqrt(ball.dx * ball.dx + ball.dy * ball.dy)
end

function handleBallBorderCollisions(ball, sounds)
    -- detect upper and lower screen boundary collision, playing a sound
    -- effect and reversing dy if true
    if ball.y <= 0 then
        ball.y = 0
        ball.dy = -ball.dy
        if sounds then
            sounds['wall_hit']:play()
        end
    end
    -- -4 to account for the ball's size
    if ball.y >= VIRTUAL_HEIGHT - 4 then
        ball.y = VIRTUAL_HEIGHT - 4
        ball.dy = -ball.dy
        if sounds then
            sounds['wall_hit']:play()
        end
    end
end

function AIControllerSimple(paddle, ball)
    local setUp = false
    local setDown = false
    local ballYmid = ball.y + (ball.height * 0.5)
    local paddleYmid = paddle.y + (paddle.height * 0.5)
    local diff = ballYmid - paddleYmid
    if diff > AI_MOVEMENT_TOLERANCE then
        setDown = true
    elseif diff < -AI_MOVEMENT_TOLERANCE then
        setUp = true
    end
    return setUp, setDown
end

AI_ADVANCED_SIMULATED_FRAMES = 10000
AI_ADVANCED_SIMULATION_DT = 0.0167
AI_ADVANCED_NO_PREDICTION_ZONE_H = 100
AIComputedFrames = 0
AIWaitFrames = 0
AITargetY = 0

function AIControllerAdvanced(paddle, ball)
    if AIWaitFrames > 0 then
        AIWaitFrames = AIWaitFrames - 1
        return false, false
    end
    local halfScreenW = VIRTUAL_WIDTH * 0.5
    local isLeftPaddle = paddle.x < halfScreenW
    if (gameState ~= 'play') or
        (isLeftPaddle and ball.dx > 0) or
        (not isLeftPaddle and ball.dx < 0) then
        AIComputedFrames = 0
        AITargetY = 0
        AIWaitFrames = 0
        return AIControllerSimple(paddle, ball)
    end
    if AIComputedFrames > 0 then
        AIComputedFrames = AIComputedFrames - 1
    else
        --[[
        local halfScreenH = VIRTUAL_HEIGHT * 0.5
        if math.abs(paddle.y - halfScreenH) <= AI_ADVANCED_NO_PREDICTION_ZONE_H then
            AITargetY = 0
            return AIControllerSimple(paddle, ball)     
        end
        ]]--
        local myball = Ball(ball.x, ball.y, ball.width, ball.height)
        myball.dx = ball.dx
        myball.dy = ball.dy
        local done = false
        while not done and (AIComputedFrames < AI_ADVANCED_SIMULATED_FRAMES) do
            handleBallBorderCollisions(myball, nil)
            myball:update(AI_ADVANCED_SIMULATION_DT)
            if isLeftPaddle then
                local closestXPaddle = paddle.x + paddle.width
                done = myball.x <= closestXPaddle
            else
                done = myball.x >= paddle.x
            end
            AIComputedFrames = AIComputedFrames + 1
        end
        AITargetY = myball.y + myball.height * 0.5
        -- Now, shift target position to try to hit the ball not with the center of the pad
        local halfPaddleH = paddle.height * 0.5
        local paddleYmid = paddle.y + halfPaddleH
        if (math.abs(paddleYmid - AITargetY)) > halfPaddleH then
            if AITargetY < paddleYmid then
                AITargetY = AITargetY + halfPaddleH * 0.7
            else
                AITargetY = AITargetY - halfPaddleH * 0.7
            end
        end
        -- Also, if we have time, do not move immmediately but wait some cycles
        --  in order to reach target position with maximum momentum
        local distance = math.abs(paddleYmid - AITargetY)
        local requiredFrames = distance / (AI_ADVANCED_SIMULATION_DT * PADDLE_SPEED)
        if AIComputedFrames > requiredFrames then
            AIWaitFrames = AIComputedFrames - requiredFrames
        end
    end

    local setUp = false
    local setDown = false
    local paddleYmid = paddle.y + (paddle.height * 0.5)
    local diff = AITargetY - paddleYmid
    if diff > AI_MOVEMENT_TOLERANCE then
        setDown = true
    elseif diff < -AI_MOVEMENT_TOLERANCE then
        setUp = true
    end
    return setUp, setDown
end

function keyboardController(keysmap)
    return love.keyboard.isDown(keysmap['upward']), love.keyboard.isDown(keysmap['downward'])
end

function updatePaddle(paddle, upSelected, downSelected)
    if upSelected then
        paddle.dy = -PADDLE_SPEED
    elseif downSelected then
        paddle.dy = PADDLE_SPEED
    else
        paddle.dy = 0
    end
end

function computeVelBouncing(paddle, ball)
    local halfBallH = ball.height * 0.5
    local ballMidY = ball.y + halfBallH
    local halfPaddleH = paddle.height * 0.5
    local paddleMidY = paddle.y + halfPaddleH
    local sum = halfPaddleH + halfBallH -1
    local coeff = math.abs(paddleMidY - ballMidY) / sum
    local angle_rad = coeff * CB_MAX_ANGLE * DEG_TO_RAD
    local vx = math.cos(angle_rad)
    local vy = math.sin(angle_rad)
    vx = vx * base_ball_speed
    vy = vy * base_ball_speed
    return vx, vy
end

--[[
    Simple function for rendering the scores.
]]
function displayScore()
    -- score display
    love.graphics.setFont(scoreFont)
    love.graphics.print(tostring(player1Score), VIRTUAL_WIDTH / 2 - 50,
        VIRTUAL_HEIGHT / 3)
    love.graphics.print(tostring(player2Score), VIRTUAL_WIDTH / 2 + 30,
        VIRTUAL_HEIGHT / 3)
end

--[[
    Renders the current FPS.
]]
function displayFPS()
    -- simple FPS display across all states
    love.graphics.setFont(smallFont)
    love.graphics.setColor(0, 255, 0, 255)
    love.graphics.print('FPS: ' .. tostring(love.timer.getFPS()), 10, 10)
end
